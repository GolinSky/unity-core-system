﻿using CodeFramework.Runtime.BaseServices;
using UnityEngine;

namespace CodeFramework.SimpleTemplate.Scene
{
    [CreateAssetMenu(fileName = "SceneModel", menuName = "Models/SceneModel", order = 1)]
    public class SimpleSceneModel:SceneModel<SimpleSceneName>
    {
        
    }
}