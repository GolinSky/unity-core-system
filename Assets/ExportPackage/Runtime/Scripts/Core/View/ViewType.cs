﻿using System;

namespace CodeFramework.Runtime.View
{
    [Serializable]
    public enum ViewType
    {
        Default = 0,
        Ui = 1,
    }
}