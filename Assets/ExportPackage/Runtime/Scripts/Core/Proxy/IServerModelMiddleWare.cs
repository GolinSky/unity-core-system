﻿using System;

namespace CodeFramework.Runtime.Proxy
{
    public interface IServerModelMiddleWare
    {
        Uri ServerUri { get; }
    }
}