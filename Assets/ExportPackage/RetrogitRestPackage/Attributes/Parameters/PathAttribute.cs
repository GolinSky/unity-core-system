﻿namespace CodeFramework.RetrogitRestPackage.Attributes.Parameters
{
    public class PathAttribute : ValueAttribute
    {
        public PathAttribute(string value)
        {
            this.Value = value;
        }
    }
}
