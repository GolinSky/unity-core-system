﻿namespace CodeFramework.RetrogitRestPackage.Attributes.Parameters
{
    public class QueryAttribute : ValueAttribute
    {
        public QueryAttribute(string value)
        {
            this.Value = value;
        }
    }
}
