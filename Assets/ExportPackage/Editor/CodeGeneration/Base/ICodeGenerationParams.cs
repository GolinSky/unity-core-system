﻿namespace CodeFramework.Editor
{
    public interface ICodeGenerationParams
    {
        string ProjectName { get; }
        string EntityName { get; }
    }
}